package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

type HttpError struct {
	Message    string
	StatusCode int
}

func (he HttpError) Error() string {
	return fmt.Sprintf("HTTP error: %s", he.Message)
}

func httpGetString(url string, ifModifiedSince int) (string, error) {
	client := http.DefaultClient

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}

	req.Header.Set("User-Agent", userAgent)
	if ifModifiedSince > 0 {
		tm := time.Unix(int64(ifModifiedSince), 0)
		req.Header.Set("If-Modified-Since", tm.Format("Mon, 02 Jan 2006 15:04:05")+" GMT")
	}

	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	// Handle bad status codes
	if resp.StatusCode != 200 {
		return "", HttpError{resp.Status, resp.StatusCode}
	}

	// Read the body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

func httpPostString(url string, bodyReader io.Reader, username string, password string) (string, error) {
	client := http.DefaultClient

	req, err := http.NewRequest("POST", url, bodyReader)
	if err != nil {
		return "", err
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("User-Agent", userAgent)
	if username != "" && password != "" {
		req.SetBasicAuth(username, password)
	}

	//dump, err := httputil.DumpRequestOut(req, true)
	//log.Debugf("%q", dump)

	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	// Handle bad status codes
	if resp.StatusCode != 200 {
		return "", HttpError{resp.Status, resp.StatusCode}
	}

	// Read the body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}
