FROM golang:1 AS build

WORKDIR /go/src/gitlab.com/wowthing/wowthing-backend

COPY . .

# Download dependencies
RUN go get -d -v ./...

# Build the Go app
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o /go/bin/wowthing-backend .


FROM alpine:latest AS runtime
RUN apk --no-cache add ca-certificates
WORKDIR /root

COPY --from=build /go/bin/wowthing-backend .
COPY --from=build /go/src/gitlab.com/wowthing/wowthing-backend/*.json ./

CMD ["./wowthing-backend"]
